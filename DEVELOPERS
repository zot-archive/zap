For reasons which will hopefully become obvious over time, I will be migrating a few repositories to PSR-12. PSR-12 is about coding style and in particular whitespace and it conflicts with a lot of our current codebase. This migration will take time. Once the migration has completed we will be adding these constraints to our integration tests.

I will apologise in advance because I actually have a number of serious issues with these conventions and the fact that they are so bloody inconsistent internally. My other gripe is that they actually make it harder for many developers to participate in a project because they might provide working code (the primary currency of open source) only to have it knocked back because the project involved is fascist about whitespace and coding style. I've been on the receiving end of this more time than I count and left a number of promising software projects over these barriers they put in place to reduce free and open collaboration and bury it in layers of bureaucratic muck.

But that is at a project level, and what this kind of standard provides is reduced friction in cross-project collaboration and code re-use across the commons. So it's a "looking at the bigger picture" move. Anyway, we're at kind of a quiet space in development and the target projects have very few external contributors at the moment. As the saying goes, "The best time to plant an oak tree is 25 years ago. The second best time is now."





Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
1 Letterman Drive
Suite D4700
San Francisco, CA, 94129

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
